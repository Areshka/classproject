<?php

class Circle extends Figure
{
    const  PI = 3.14;
    protected $radius;

    public function __construct($radius)
    {
        $this->radius = $radius;
    }

    public function getPerimetr()
    {
        $this->perimetr = 2 * self::PI * $this->radius;
        return $this->perimetr;
    }

    public function getSquare() {
        $this->square = self::PI * pow($this->radius, 2);
        return $this->square;
    }
}