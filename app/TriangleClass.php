<?php

class Triangle extends Figure
{
    protected $side_a;
    protected $side_b;
    protected $side_c;
    protected $error;

    public function __construct($side_a, $side_b, $side_c)
    {
        $this->a = $side_a;
        $this->b = $side_b;
        $this->c = $side_c;
    }

    public function getPerimetr()
    {
        $this->perimetr = $this->a + $this->b + $this->c;
        return $this->perimetr;
    }

    public  function getSquare()
    {
        $this->square = sqrt($this->perimetr * ($this->perimetr - $this->a) * ($this->perimetr - $this->b) * ($this->perimetr - $this->c));
        return $this->square;
    }

}