<?php

class Rectangle extends Figure
{
    protected $side_a;
    protected $side_b;
    protected $side_c;
    protected $side_d;

    public function __construct($side_a, $side_b, $side_c, $side_d)
    {
        $this->a = $side_a;
        $this->b = $side_b;
        $this->c = $side_c;
        $this->d = $side_d;
    }

    public function getPerimetr()
    {
        $this->perimetr = $this->a + $this->b + $this->c + $this->d;
        return $this->perimetr;
    }

    public function getSquare()
    {
        $this->square = $this->a * $this->b;
        return $this->square;
    }
}