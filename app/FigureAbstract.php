<?php

abstract class Figure{

    public $perimetr = 0;

    public $square = 0;

    abstract public function getPerimetr();

    abstract public function getSquare();
}