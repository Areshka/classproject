<?php

define('APP_DIR', __DIR__ . '/../app');
require_once APP_DIR . '/FigureAbstract.php';
require_once APP_DIR . '/TriangleClass.php';
require_once APP_DIR . '/SquareClass.php';
require_once APP_DIR . '/RectangleClass.php';
require_once APP_DIR . '/CircleClass.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$figure = $_POST['figure'];
	$side_a = $_POST['a'];
	$side_b = $_POST['b'];
	$side_c = $_POST['c'];
	$side_d = $_POST['d'];
	$radius = $_POST['radius'];

	if ($side_a && $side_b && $side_c && $figure == "triangle") {
		$triangle = new Triangle($side_a, $side_b, $side_c);
		if (($side_a < $side_b + $side_c) && ($side_b < $side_a + $side_c) && ($side_c < $side_a + $side_b)) {
			echo "The perimeter of the triangle with sides: a = " . $side_a . ", b =  " . $side_b . ", c = " . $side_c . " equal " .  $triangle->getPerimetr() .
			" and square equal " . $triangle->getSquare();
		} else {
			echo "Triangle does not exist!!!";
		}
	}

	if ($side_a && $side_b && $side_c && $side_d && $figure == "square") {
		$square = new Square($side_a, $side_b, $side_c, $side_d);
		if ($side_a == $side_b && $side_b == $side_c && $side_c == $side_d && $side_d == $side_a) {
			echo "The perimeter of the square with sides: a = " . $side_a . ", b =  " . $side_b . ", c = " . $side_c . ", d = " . $side_d . " equal " .
			$square->getPerimetr() . " and squre equal " . $square->getSquare();
		} else {
			echo "Square does not exist!!!";
		}
	}

	if ($side_a && $side_b && $side_c && $side_d && $figure == "rectangle") {
		$rectangle = new Rectangle($side_a, $side_b, $side_c, $side_d);
		$diagonal_ab = sqrt(pow($side_a, 2) + pow($side_b, 2));
		$diagonal_cd = sqrt(pow($side_c, 2) + pow($side_d, 2));
		if ($diagonal_ab == $diagonal_cd) {
			echo "The perimeter of the rectangle with sides: a = " . $side_a . ", b =  " . $side_b . ", c = " . $side_c . ", d = " . $side_d . " equal " .
			$rectangle->getPerimetr() . " and square equal " . $rectangle->getSquare();
		}
	}

	if ($radius && $figure == "circle") {
		$circle = new Circle($radius);
		echo "The perimeter of the circle with radius " . $radius . " equal " . $circle->getPerimetr() . " and square equal " . $circle->getSquare();
	}

}

?>

<form action="<?= $_SERVER['PHP_SELF']; ?>" method="POST">
	Select figure:
	<select name="figure">
		<option name="triangle" value="triangle" <?php echo ($figure == 'triangle') ? "selected" : ""; ?>>Triangle</option>
		<option name="square" value="square" <?php echo ($figure == 'square') ? "selected" : ""; ?>>Square</option>
		<option name="rectangle" value="rectangle" <?php echo ($figure == 'rectangle') ? "selected" : ""; ?>>Rectangle</option>
		<option name="circle" value="circle" <?php echo ($figure == 'circle') ? "selected" : ""; ?>>Circle</option>
	</select>
	<br><br>
	<?php if ($figure == "triangle") : ?>
		Enter side a: <input type="text" name="a"><br>
		Enter side b: <input type="text" name="b"><br>
		Enter side c: <input type="text" name="c"><br><br>
	<?php elseif(($figure == "square") or ($figure == "rectangle")) : ?>
		Enter side a: <input type="text" name="a"><br>
		Enter side b: <input type="text" name="b"><br>
		Enter side c: <input type="text" name="c"><br>
		Enter side d: <input type="text" name="d"><br><br>
	<?php endif; ?>
	<?php if ($figure == "circle") : ?>
		Enter radius: <input type="text" name="radius"><br><br>
	<?php endif; ?>

	<input type="submit" value="Send">
</form>

